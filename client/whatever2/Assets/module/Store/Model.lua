local m = {}

m.points = {}

m.hanPieces = {}  -- 汉

m.chuPieces = {}  -- 楚

local config = require("module/Store/Config")
m.emptyPieceName = config.emptyPieceName

for k,v in pairs(config.points) do
	local rowPoints = {}
	for ii,vv in ipairs(v) do
		rowPoints[ii] = { id=0,name=m.emptyPieceName,row = k,column=ii }
	end
	m.points[k] = rowPoints
end

for i,v in ipairs(config.pieces)do
	local tb = { id=v.id,name=v.name,initColumn=v.initColumn,initRow=v.initRow,normalColumn=v.normalColumn,normalRow=v.normalRow }
	tb.column = v.normalColumn
	tb.row = v.normalRow
	if i<=16 then
		table.insert(m.hanPieces,tb)
	else
		table.insert(m.chuPieces,tb)
	end
	m.points[tb.row][tb.column].id = tb.id
	m.points[tb.row][tb.column].name = tb.name
end

return m