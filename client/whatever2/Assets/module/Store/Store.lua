local x = {}
x.version = 1
x.files = {
	["Controller"] = "module/Store/Controller",
	["Model"] = "module/Store/Model",
	["View"] = "module/Store/View",

	["Config"] = "module/Store/Config",
	["Net"] = "module/Store/Net",
	["Define"] = "module/Store/Define",
}

x.Load = function()
	x.Controller = require(x.files.Controller)
	x.Model = require(x.files.Model)
	x.View = require(x.files.View)

	x.Config = require(x.files.Config)
	x.Net = require(x.files.Net)
	x.Define = require(x.files.Define)
	PrintLoaded()
end

x.Unload = function()
	package.loaded[x.files.Controller] = nil
	package.loaded[x.files.Model] = nil
	package.loaded[x.files.View] = nil

	package.loaded[x.files.Config] = nil
	package.loaded[x.files.Net] = nil
	package.loaded[x.files.Define] = nil

	x.Controller = nil
	x.Model = nil
	x.View = nil

	x.Config = nil
	x.Net = nil
	x.Define = nil
	PrintLoaded()
end

return x