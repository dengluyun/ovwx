local x = {}

LBeat = function()
	print("DriveBeat from C#")
end
LRaycast = function(go)
	local name = go.name
	print("DriveRaycast name:"..name)
	local parentName = go.transform.parent.name
	if parentName=="Chess" then
		local strs = split(name,'_')
		local row = tonumber(strs[1])
		local column = tonumber(strs[2])
		Totem.Controller.OnClickPoint(row,column)
	end
end

LScrollCellAtIndex = function(go)
	local name = go.name
	local n = string.gsub(name, "Cell", "")
	print("DriveScrollCellAtIndex name:"..name..","..n)
	local nn = tonumber(n)+1
	local b = not callback2 or callback2(go,nn)
end


------------------------------ network ------------------------------
LLOnGetSuccess = function(requestText)
	print("OnGetSuccess:",string.len(requestText),requestText)
end
LLOnGetFailed = function(requestError)
	print("OnGetFailed:",requestError)
end
LLOnGetProgress = function(progress)
	print("OnGetProgress",progress)
end
LLOnGetComplete = function(requestText)
	print("OnGetComplete",requestText)
end

-- local inviteImage = inviteTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))

LLOnGetImageSuccess= function(texture2d)
	print("OnGetImageSuccess",texture2d)
	local rect = CS.UnityEngine.Rect(0,0,100,100)
	local vec = CS.UnityEngine.Vector2(0.5,0.5)
	inviteImage.sprite = CS.UnityEngine.Sprite.Create(texture2d,rect,vec)
	inviteImage:SetNativeSize()
end

-- CS.Whatever.Elevator.CoRequestGet("http://www.baidu.com",OnGetSuccess,OnGetFailed,OnGetProgress,OnGetComplete)
local m = "F:/ovwx/client/whatever2/Assets/Demo/Icon/Box1.png"
-- CS.Whatever.Elevator.CoRequestGetTexture(m,OnGetImageSuccess,OnGetFailed,OnGetProgress,OnGetComplete)

LLOnGetAssetBundleSuccess= function(assetBundle)
  print("OnGetAssetBundleSuccess:",assetBundle.name)
  local s1 = assetBundle:LoadAsset("Icon1.png",typeof(CS.UnityEngine.Sprite))
  local s2 = assetBundle:LoadAsset("Icon2.png",typeof(CS.UnityEngine.Sprite))
  local s3 = assetBundle:LoadAsset("SubCell.prefab")

  inviteImage.sprite = CS.UnityEngine.GameObject.Instantiate(s1)
  inviteImage:SetNativeSize()
  local obj = CS.UnityEngine.GameObject.Instantiate(s3)
  obj.transform:SetParent(CS.Whatever.App.Instance.transform)
end

local b = "F:/ovwx/client/whatever2/Assets/Bundles/art"
-- CS.Whatever.Elevator.CoRequestGetAssetbundle(b,LLOnGetAssetBundleSuccess,LLOnGetFailed,LLOnGetProgress,LLOnGetComplete)

LLOnGetAudioClipSuccess= function(audioClip)
  print("OnGetAudioClipSuccess:",audioClip.name)  
end

return x