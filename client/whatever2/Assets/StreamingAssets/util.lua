local x = {}
------------------------------ normal ------------------------------
callback = nil
callback1 = nil
callback2 = nil
callback3 = nil

SetScrollCellCallback = function(fun)
    callback2 = fun
end

SetButtonAction = function(button,func)
	button.onClick:RemoveAllListeners()
	button.onClick:AddListener(function()
		func(button.gameObject)
	end)
end

SetImageSprite = function(image,name)
	image.sprite = CS.UnityEngine.Resources.Load(name,typeof(CS.UnityEngine.Sprite))
end


---@param hex string
HexToRGB = function(hex)
	if type(hex) ~= "string" then
		return 1, 1, 1
	end
	if IsNullOrEmpty(hex) then
		return 1, 1, 1
    end

    if string.len(hex) < 6 then
		error("hexToColor param error: hex = " .. tostring(hex))
		return 1, 1, 1
	end

	hex = string.gsub(hex, "0x", "")
	local s
	s = string.sub(hex, 1, 2)
	local r = tonumber(s, 16) / 255
	s = string.sub(hex, 3, 4)
	local g = tonumber(s, 16) / 255
	s = string.sub(hex, 5, 6)
	local b = tonumber(s, 16) / 255
	return r, g, b
end

NewGuid = function()
	local g = CS.System.Guid.NewGuid()
	--print("NewGuid:"..g:ToString())
	--print("NewGuid:"..g:ToString("d"))
	print("NewGuid:"..g:ToString("n"))
	--print("NewGuid:"..g:ToString("b"))
	--print("NewGuid:"..g:ToString("p"))
end
------------------------------ normal ------------------------------

------------------------------ misc ------------------------------
function IsNullOrEmpty(str)
	if str == nil or str == "" then
		return true
	end
	return false
end

PrintLoaded = function ()
	print("PrintLoaded")
	for k,v in pairs(package.loaded) do
		print(k,v)
	end
end

function print_r ( t )
	local print_r_cache={}
	local function sub_print_r(t,indent)
		if (print_r_cache[tostring(t)]) then
			print(indent.."*"..tostring(t))
		else
			print_r_cache[tostring(t)]=true
			if (type(t)=="table") then
				for pos,val in pairs(t) do
					if (type(val)=="table") then
						print(indent.."["..pos.."] => "..tostring(t).." {")
						sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
						print(indent..string.rep(" ",string.len(pos)+6).."}")
					elseif (type(val)=="string") then
						print(indent.."["..pos..'] => "'..val..'"')
					else
						print(indent.."["..pos.."] => "..tostring(val))
					end
				end
			else
				print(indent..tostring(t))
			end
		end
	end
	if (type(t)=="table") then
		print(tostring(t).." {")
		sub_print_r(t,"  ")
		print("}")
	else
		sub_print_r(t,"  ")
	end
	print()
end

table.print = print_r

function PrintTable( tbl , level, filteDefault)
	local msg = ""
	filteDefault = filteDefault or true --Ĭ�Ϲ��˹ؼ��֣�DeleteMe, _class_type��
	level = level or 1
	local indent_str = ""
	for i = 1, level do
		indent_str = indent_str.."  "
	end

	print(indent_str .. "{")
	for k,v in pairs(tbl) do
		if filteDefault then
			if k ~= "_class_type" and k ~= "DeleteMe" then
				local item_str = string.format("%s%s = %s", indent_str .. " ",tostring(k), tostring(v))
				print(item_str)
				if type(v) == "table" then
					PrintTable(v, level + 1)
				end
			end
		else
			local item_str = string.format("%s%s = %s", indent_str .. " ",tostring(k), tostring(v))
			print(item_str)
			if type(v) == "table" then
				PrintTable(v, level + 1)
			end
		end
	end
	print(indent_str .. "}")
end


function split(str, split_char)
	local sub_str_tab = {}
	while true do
		local pos = string.find(str, split_char)
		if not pos then
			table.insert(sub_str_tab,str)
			break
		end
		local sub_str = string.sub(str, 1, pos - 1)
		table.insert(sub_str_tab,sub_str)
		str = string.sub(str, pos + 1, string.len(str))
	end
	return sub_str_tab
end
------------------------------ misc ------------------------------

return x