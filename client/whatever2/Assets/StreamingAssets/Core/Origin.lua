local os = require 'os'
local string = require 'string'
local math = require 'math'
local debug = require 'debug'

local table = require 'table'


local json = require "json"
local words = require "words"

local util = require("util")
local define = require("define")

local elevator = require("elevator")

-- package.path = package.path .. ";../?.lua"
NewGuid()
local canvasTransform = CS.UnityEngine.GameObject.Find("Canvas").transform
local middleBackgroundTransform = canvasTransform:Find("Middle/Background"):GetComponent(typeof(CS.UnityEngine.UI.Image))
local bottomBackgroundTransform = canvasTransform:Find("Bottom/Background"):GetComponent(typeof(CS.UnityEngine.UI.Image))
local mainCamera = CS.UnityEngine.Camera.main
local homeTransform = canvasTransform:Find("Bottom/Home")
local rankTransform = canvasTransform:Find("Bottom/Rank")
local nearbyTransform = canvasTransform:Find("Bottom/Nearby")
local searchTransform = canvasTransform:Find("Bottom/Search")
local cursor = homeTransform

local ChangeTheme = function()
	-- mainCamera.backgroundColor = define.middleBackgroundColor
	middleBackgroundTransform.color = define.middleBackgroundColor
	bottomBackgroundTransform.color = define.bottomBackgroundColor
	local img = homeTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))
	img.color = define.normalColor
	local txt = homeTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
	txt.color = define.normalColor

	local img = rankTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))
	img.color = define.normalColor
	local txt = rankTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
	txt.color = define.normalColor

	local img = nearbyTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))
	img.color = define.normalColor
	local txt = nearbyTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
	txt.color = define.normalColor

	local img = searchTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))
	img.color = define.normalColor
	local txt = searchTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
	txt.color = define.normalColor
end

ChangeTheme()

local SetCursor = function(go)
	if cursor then
		local img = cursor:GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))
		-- SetImageSprite(img,img.name.."N")
		img.color = define.normalColor
		local txt = cursor:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
		txt.color = define.normalColor
	end

	local img = go.transform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))
	-- SetImageSprite(img,img.name.."H")
	img.color = define.cursorColor
	local txt = go.transform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
	txt.color = define.cursorColor
	cursor = go.transform
end
SetCursor(homeTransform.gameObject)

local titleText = canvasTransform:Find("Title"):GetComponent(typeof(CS.TMPro.TextMeshProUGUI))

local homeText = homeTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
homeText.text = "往事"--words.home
titleText.text = homeText.text
local homeButton = homeTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Button))
SetButtonAction(homeButton,function(go)
	SetCursor(go)
	titleText.text = homeText.text--words.project
end)

local rankText = rankTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
rankText.text = "你"--words.applet
local rankButton = rankTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Button))
SetButtonAction(rankButton,function(go)
	SetCursor(go)
	titleText.text = rankText.text--words.applet
end)
local nearbyText = nearbyTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
nearbyText.text = "我"--words.gamelet
local nearbyButton = nearbyTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Button))
SetButtonAction(nearbyButton,function(go)
	SetCursor(go)
	titleText.text = nearbyText.text--words.gamelet
end)
local searchText = searchTransform:GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
searchText.text = "ta"--words.mine
local searchButton = searchTransform:GetComponentInChildren(typeof(CS.UnityEngine.UI.Button))
SetButtonAction(searchButton,function(go)
	SetCursor(go)
	titleText.text = searchText.text--words.mine
	SetScrollCellCallback(require("Core/Order").ScrollCellIndex)
end)


ModuleDownload = function(name)
end

ModuleInstall = function(name)
	local dn = CS.UnityEngine.Application.persistentDataPath.."/module/"..name
	local d = dn.."/"
	if CS.System.IO.Directory.Exists(d) then
		CS.System.IO.Directory.Delete(d,true)
	end

	CS.System.IO.Directory.CreateDirectory(d)
	local fs = CS.System.IO.Directory.GetFiles(d)
	for i=1,fs.Length do
		local fn = fs[i-1]
		local n = CS.System.IO.Path.GetFileName(fn)
		print(n,fn)
		CS.System.IO.File.Copy(fn,d.."/"..n)
	end
end

ModuleUpdate = function(name)
end

ModuleUninstall = function(name)
	local dn = CS.UnityEngine.Application.persistentDataPath.."/module/"..name
	local d = dn.."/"
	if CS.System.IO.Directory.Exists(d) then
		CS.System.IO.Directory.Delete(d,true)
	end
end

Totem = nil
local bInstalled = false
local OnDoSomething = function()
	if bInstalled then
		local n = "module/Store/Store"
		require(n).Unload()
		ModuleUninstall("Store")
		bInstalled = false
	else
		ModuleInstall("Store")
		bInstalled = true
		local n = "module/Store/Store"
		Totem = require(n)
		Totem.Load()
	end
end
-- OnDoSomething()

local pray = [=[
-----------------------------------------------
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
         (#DMM-104360)-(global_develop)-[18046603032]-[add]-限时礼包优化手动合海外

         0c0a473c74e043f087defcc3f3d2de0e
]=]
print(pray)