local x = {}
local words = require "words"

-- 1 normal 2 takedown
x.list = {
	{name="愚公移山",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="精卫填海",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="夸父追日",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	
	{name="鼠目寸光",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="牛毛细雨",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="虎视眈眈",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="兔走乌飞",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},

	{name="龙凤呈祥",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="蛇口蜂针",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="马到成功",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="羊肠九曲",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	
	{name="猴子救月",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="鸡毛蒜皮",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="狗皮膏药",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
	{name="猪突豨勇",num=0,time=0,icon="icon",posters="posters",summary="summary",period=1,vendor=words.venderIdeaAuthor},
}

return x