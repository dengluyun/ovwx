local x = {}
local words = require "words"
------------------------------- phase II -------------------------------
x.state = {
	creating,
	doing,
	checking,
	arraigning,
	preparing,
	playable,
	unavailable,
}

x.CreateItem = function(name)
	local r = {}
	r.guid = NewGuid()
	r.id = 1001
	r.name = name
	r.type = 1	-- 1 p 2 a 3 g
	return r
end

x.mine = {
	x.CreateItem(),
	x.CreateItem(),
	x.CreateItem(),
}

x.shelves = {
	x.CreateItem(),
	x.CreateItem(),
	x.CreateItem(),
	x.CreateItem(),
	x.CreateItem(),
	x.CreateItem(),
}


x.ScrollCellIndex = function(go,lIndex)
	print("order ScrollCellIndex idx:"..lIndex)
	local n = (lIndex%12) + 1
	local vc = go:GetComponent(typeof(CS.Whatever.ViewCell))
	vc.svgImage.sprite = CS.UnityEngine.Resources.Load("ZodiacBit"..n,typeof(CS.UnityEngine.Sprite))

	if lIndex==1 then
		local rTran = go:GetComponent(typeof(CS.UnityEngine.RectTransform))
		rTran.sizeDelta = CS.UnityEngine.Vector2(720,64)
		local top = go.transform:Find("ImageLineTop"):GetComponent(typeof(CS.UnityEngine.RectTransform))
		top.sizeDelta = CS.UnityEngine.Vector2(720,2)
		local bottom = go.transform:Find("ImageLineBottom"):GetComponent(typeof(CS.UnityEngine.RectTransform))
		bottom.sizeDelta = CS.UnityEngine.Vector2(720,2)
		local t = go.transform:Find("Title"):GetComponent(typeof(CS.TMPro.TextMeshProUGUI))
		t.text = "待办"
	elseif lIndex==2 then
		local top = go.transform:Find("ImageLineTop"):GetComponent(typeof(CS.UnityEngine.RectTransform))
		top.gameObject:SetActive(false)
		local bottom = go.transform:Find("ImageLineBottom"):GetComponent(typeof(CS.UnityEngine.RectTransform))
		bottom.gameObject:SetActive(false)
		local t = go.transform:Find("Normal/Button"):GetComponentInChildren(typeof(CS.TMPro.TextMeshProUGUI))
		t.text = "归还"
		local img = go.transform:Find("Normal/Icon/Bg"):GetComponentInChildren(typeof(CS.UnityEngine.UI.Image))
		local r,g,b = HexToRGB("fa5151")
		t.color = CS.UnityEngine.Color(r,g,b,1)
		r,g,b = HexToRGB("00c25f")
		img.color = CS.UnityEngine.Color(r,g,b,1)
	elseif lIndex==3 then
		local rTran = go:GetComponent(typeof(CS.UnityEngine.RectTransform))
		rTran.sizeDelta = CS.UnityEngine.Vector2(720,64)
		local top = go.transform:Find("ImageLineTop"):GetComponent(typeof(CS.UnityEngine.RectTransform))
		top.sizeDelta = CS.UnityEngine.Vector2(720,2)
		local bottom = go.transform:Find("ImageLineBottom"):GetComponent(typeof(CS.UnityEngine.RectTransform))
		bottom.sizeDelta = CS.UnityEngine.Vector2(720,2)
		local t = go.transform:Find("Title"):GetComponent(typeof(CS.TMPro.TextMeshProUGUI))
		t.text = "挂单"
	elseif lIndex==4 then
		local top = go.transform:Find("ImageLineTop"):GetComponent(typeof(CS.UnityEngine.RectTransform))
		top.gameObject:SetActive(false)
	else
	end
	go.transform:Find("Title").gameObject:SetActive(lIndex==1 or lIndex==3)
	go.transform:Find("Normal").gameObject:SetActive(lIndex~=1 and lIndex~=3)
end

return x