local x = {
	["opus"] 					= "作品",
	["product"] 				= "质品",
	["standard"] 				= "标品",


	["applet"] 					= "功能",
	["gamelet"] 				= "玩法",


	["project"] 				= "项目",


	["propose"] 				= "发起",
	["idea"] 					= "想法",
	["design"] 					= "设计",
	["make"] 					= "制作",
	["test"] 					= "测试",


	["home"] 					= "首页",
	["invite"] 					= "招标",
	["bid"] 					= "投标",
	["arraign"] 				= "提审",
	["approve"] 				= "审批",


	["leaflets"] 				= "宣单",
	["tickets"] 				= "工单",


	["mine"] 					= "我的",
	["order"] 					= "挂单",
	["claim"] 					= "认领",
	["rturn"] 					= "归还",
	["turn"] 					= "转送",


	["receipt"] 				= "确收",

	["sign"] 					= "签收",
	-- 首页 作品 排行 招标 
	-- 作品 排行 作品 开收单 

	["official"] 				= "官方",  -- 官方
	["folk"] 					= "游戏",  -- 民间
	["entity"] 					= "实体",  -- 


	["crowd"] 					= "众筹",
	["reward"] 					= "悬赏",
	["ad"] 						= "广告",
	["redpack"] 				= "红包",
	["commonweal"] 				= "公益",
	["announcement"] 			= "公告",
	["wanted"] 					= "通缉",

	["nearby"] 					= "周边",--
	["search"] 					= "查找",--民间
	["rank"] 					= "排行",


	["calendar"] 				= "日历",
	["recommend"] 				= "推荐",


	["venderIdeaAuthor"] 		= "作者发起",
	["venderIdeaLeader"] 		= "团队发起",
	["venderIdeaPerson"] 		= "民间发起",
	["venderMakeOfficial"] 		= "官方制作",
	["venderMakeTeam"] 			= "团队制作",
	["venderMakeFolk"] 			= "民间制作",


	------------------------------ verb ---------------------------------
	["unlock1001"] 				= "解锁",
	["open1002"] 				="开启",
	["satisfy1003"]				= "满足",
}

return x