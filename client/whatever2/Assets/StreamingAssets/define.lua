local x = {}

x.white = CS.UnityEngine.Color(1,1,1,1)
local r,g,b = HexToRGB("e0e0e0")
x.pwhite = CS.UnityEngine.Color(r,g,b,1)
r,g,b = HexToRGB("191919")
x.black = CS.UnityEngine.Color(r,g,b,1)
r,g,b = HexToRGB("222222")
x.pblack = CS.UnityEngine.Color(r,g,b,0.98)

x.porange = CS.UnityEngine.Color(1,0.25,0,0.8)
x.pyellow = CS.UnityEngine.Color(1,1,0,0.8)
r,g,b = HexToRGB("57bf6a")
x.pgreen = CS.UnityEngine.Color(r,g,b,1)
x.pblue = CS.UnityEngine.Color(0,103/255,149/255,0.8)
x.noColor = x.white

local theme = 1 -- 1 light 2 dark
SetTheme = function(t)
	theme = t
	if theme==1 then
		x.middleBackgroundColor = x.white
		x.bottomBackgroundColor = x.pwhite
		x.normalColor = x.black
		x.cursorColor = x.pblue
	else
		x.middleBackgroundColor = x.black
		x.bottomBackgroundColor = x.pblack
		x.normalColor = x.pwhite
		x.cursorColor = x.pgreen
	end 
end

SetTheme(2)

return x