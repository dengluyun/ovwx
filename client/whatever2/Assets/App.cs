﻿namespace Whatever
{
    public class App : UnityEngine.MonoBehaviour
    {
        static XLua.LuaEnv luaEnv;
        static System.Action<UnityEngine.GameObject> LRaycast;


        public static App Instance;
        static App()
        {
            luaEnv=new XLua.LuaEnv();
            luaEnv.AddLoader(LuaLoader);
        }

        static byte[] LuaLoader(ref string fileName)
        {
            string fullPath = UnityEngine.Application.streamingAssetsPath+@"/"+fileName+".lua";
            if(System.IO.File.Exists(fullPath))
            {
                var s = System.IO.File.ReadAllText(fullPath);
                return System.Text.Encoding.UTF8.GetBytes(s);
            }
            else
                Debug.LogError("load lua script failed ,fullname:"+fullPath);

#if UNITY_EDITOR
            fullPath=UnityEngine.Application.dataPath+@"/"+fileName+".lua";
#else
            fullPath = UnityEngine.Application.persistentDataPath+@"/"+fileName+".lua";
#endif
            if(System.IO.File.Exists(fullPath))
            {
                var s = System.IO.File.ReadAllText(fullPath);
                return System.Text.Encoding.UTF8.GetBytes(s);
            }
            else
                Debug.LogError("load lua module script failed ,fullname:"+fullPath);

            return null;
        }

        void OnDestroy()
        {
        }

        void Awake()
        {
            Instance=this;
        }

        void Start()
        {
            if(!System.IO.Directory.Exists(Define.moduleLocalDirectory))
                System.IO.Directory.CreateDirectory(Define.moduleLocalDirectory);
            luaEnv.DoString("require 'Core/Origin' ");

            luaEnv.Global.Get("LRaycast",out LRaycast);
            luaEnv.Global.Get("LScrollCellAtIndex",out ViewCell.LScrollCellAtIndex);
        }

        void Update()
        {
            if(UnityEngine.Input.GetMouseButtonDown(0))
            {
                UnityEngine.RaycastHit hit;
                var ray = UnityEngine.Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
                if(UnityEngine.Physics.Raycast(ray,out hit))
                    LRaycast(hit.collider.gameObject);
            }
        }

    }

}