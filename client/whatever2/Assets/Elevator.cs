﻿namespace Whatever
{
    public class Elevator
    {
        //***************************** http *****************************//
        public static void CoRequestGetAssetbundle(string url,System.Action<UnityEngine.AssetBundle> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            App.Instance.StartCoroutine(RequestGetAssetBundle(url,onSuccess,onFail,onProgress,onComplete));
        }

        public static void CoRequestGetAudioClip(string url,System.Action<UnityEngine.AudioClip> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            App.Instance.StartCoroutine(RequestGetAudioClip(url,onSuccess,onFail,onProgress,onComplete));
        }

        public static void CoRequestGetTexture(string url,System.Action<UnityEngine.Texture2D> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            App.Instance.StartCoroutine(RequestGetTexture(url,onSuccess,onFail,onProgress,onComplete));
        }

        public static void CoRequestGet(string url,System.Action<string> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            App.Instance.StartCoroutine(RequestGet(url,onSuccess,onFail,onProgress,onComplete));
        }

        public static void CoRequestPost(string url,string data,System.Action<string> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            App.Instance.StartCoroutine(RequestPost(url,data,onSuccess,onFail,onProgress,onComplete));
        }

        public static System.Collections.IEnumerator RequestGetAssetBundle(string url,System.Action<UnityEngine.AssetBundle> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            using(UnityEngine.Networking.UnityWebRequest webRequest = UnityEngine.Networking.UnityWebRequestAssetBundle.GetAssetBundle(url))
            {
                webRequest.SendWebRequest();

                while(!webRequest.isDone)
                {
                    onProgress?.Invoke(webRequest.downloadProgress.ToString());
                    yield return null;
                }

                if(webRequest.result==UnityEngine.Networking.UnityWebRequest.Result.Success)
                    onSuccess?.Invoke(UnityEngine.Networking.DownloadHandlerAssetBundle.GetContent(webRequest));
                else
                    onFail?.Invoke(webRequest.error);

                yield return null;
                onComplete?.Invoke(webRequest.url);
            }
        }

        public static System.Collections.IEnumerator RequestGetAudioClip(string url,System.Action<UnityEngine.AudioClip> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            using(UnityEngine.Networking.UnityWebRequest webRequest = UnityEngine.Networking.UnityWebRequestTexture.GetTexture(url,false))
            {
                webRequest.SendWebRequest();

                while(!webRequest.isDone)
                {
                    onProgress?.Invoke(webRequest.downloadProgress.ToString());
                    yield return null;
                }

                if(webRequest.result==UnityEngine.Networking.UnityWebRequest.Result.Success)
                    onSuccess?.Invoke(UnityEngine.Networking.DownloadHandlerAudioClip.GetContent(webRequest));
                else
                    onFail?.Invoke(webRequest.error);

                yield return null;
                onComplete?.Invoke(webRequest.url);
            }
        }

        public static System.Collections.IEnumerator RequestGetTexture(string url,System.Action<UnityEngine.Texture2D> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            using(UnityEngine.Networking.UnityWebRequest webRequest = UnityEngine.Networking.UnityWebRequestTexture.GetTexture(url,false))
            {
                webRequest.SendWebRequest();

                while(!webRequest.isDone)
                {
                    onProgress?.Invoke(webRequest.downloadProgress.ToString());
                    yield return null;
                }

                if(webRequest.result==UnityEngine.Networking.UnityWebRequest.Result.Success)
                    onSuccess?.Invoke(UnityEngine.Networking.DownloadHandlerTexture.GetContent(webRequest));
                else
                    onFail?.Invoke(webRequest.error);

                yield return null;
                onComplete?.Invoke(webRequest.url);
            }
        }

        public static System.Collections.IEnumerator RequestGet(string url,System.Action<string> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            using(UnityEngine.Networking.UnityWebRequest webRequest = UnityEngine.Networking.UnityWebRequest.Get(url))
            {
                webRequest.disposeCertificateHandlerOnDispose=false;
                webRequest.SendWebRequest();

                while(!webRequest.isDone)
                {
                    onProgress?.Invoke(webRequest.downloadProgress.ToString());
                    yield return null;
                }

                if(webRequest.result==UnityEngine.Networking.UnityWebRequest.Result.Success)
                    onSuccess?.Invoke(webRequest.downloadHandler.text);
                else
                    onFail?.Invoke(webRequest.error);

                yield return null;
                onComplete?.Invoke(webRequest.downloadHandler.text);
            }
        }

        public static System.Collections.IEnumerator RequestPost(string url,string data,System.Action<string> onSuccess = null,System.Action<string> onFail = null,System.Action<string> onProgress = null,System.Action<string> onComplete = null)
        {
            UnityEngine.WWWForm form = new UnityEngine.WWWForm();
            form.AddField("key","value");
            form.AddField("name","dely");
            using(UnityEngine.Networking.UnityWebRequest webRequest = UnityEngine.Networking.UnityWebRequest.Post(url,form))
            {
                webRequest.disposeCertificateHandlerOnDispose=false;
                webRequest.SendWebRequest();

                while(!webRequest.isDone)
                {
                    onProgress?.Invoke(webRequest.downloadProgress.ToString());
                    yield return null;
                }

                if(webRequest.result==UnityEngine.Networking.UnityWebRequest.Result.Success)
                    onSuccess?.Invoke(webRequest.downloadHandler.text);
                else
                    onFail?.Invoke(webRequest.error);

                yield return null;
                onComplete?.Invoke(webRequest.downloadHandler.text);
            }
        }
        //***************************** http *****************************//
    }

}