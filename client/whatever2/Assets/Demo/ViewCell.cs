﻿namespace Whatever
{
    public class ViewCell : UnityEngine.MonoBehaviour
    {
        public static System.Action<UnityEngine.GameObject> LScrollCellAtIndex;

        public UnityEngine.UI.Image topLine;
        public UnityEngine.UI.Image bottomLine;
        public TMPro.TextMeshProUGUI textMeshProUGUI;
        public UnityEngine.Sprite sprite;
        public Unity.VectorGraphics.SVGImage svgImage;
        // Start is called before the first frame update
        void ScrollCellIndex(int idx)
        {
            textMeshProUGUI.text = "cell at:" + idx;
            gameObject.name = "Cell"+idx;
            LScrollCellAtIndex?.Invoke(gameObject);
        }
 
    }

}